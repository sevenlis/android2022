package ru.clevertec.sevenlis.android2022;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    final List<Item> itemList;
    final OnRecyclerViewInteractionListener mListener;
    final Context mContext;

    public RecyclerViewAdapter(List<Item> itemList, OnRecyclerViewInteractionListener mListener, Context mContext) {
        this.itemList = itemList;
        this.mListener = mListener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setItem(itemList.get(position));
        holder.getTvTitle().setText(holder.getItem().getTitle());
        holder.getTvDescription().setText(holder.getItem().getDescription());

        holder.getItemView().setOnClickListener(view -> mListener.onListItemSelected(holder.getItem(), holder.getItemView()));

        ViewCompat.setTransitionName(holder.getImageView(), "image_view_" + position);
        ViewCompat.setTransitionName(holder.getTvTitle(), "text_view_title_" + position);
        ViewCompat.setTransitionName(holder.getTvDescription(), "text_view_description_" + position);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private Item item;
        private final View itemView;
        private final ImageView imageView;
        private final TextView tvTitle;
        private final TextView tvDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.imageView = getItemView().findViewById(R.id.imageView);
            this.tvTitle = getItemView().findViewById(R.id.title);
            this.tvDescription = getItemView().findViewById(R.id.description);
        }

        public View getItemView() {
            return this.itemView;
        }

        public Item getItem() {
            return item;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public TextView getTvTitle() {
            return tvTitle;
        }

        public TextView getTvDescription() {
            return tvDescription;
        }

        public void setItem(Item item) {
            this.item = item;
        }
    }

    interface OnRecyclerViewInteractionListener {
        void onListItemSelected(Item item, View itemView);
    }
}
